## Creality CR-10

### Connecting through Arduino as ISP

Downloading firmware from board

    avrdude -p m1284p -c stk500v1 -b19200 -P /dev/tty.wchusbserial1420 -U flash:r:flash.bin:r
    avrdude -p m1284p -c stk500v1 -b19200 -P /dev/tty.wchusbserial1420 -U eeprom:r:eeprom.eep:i